import './App.css';
import React, { useEffect } from 'react';
import Create from './components/create';
import Read from './components/read';
import Update from './components/update';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom'
import { Button } from 'semantic-ui-react';


function App() {
  useEffect(()=>{
    console.log(window._env_.API_URL)
  },[])
  return (
    
    <Router>
      <div className="main">
        <h2 className="main-header">React Crud Operations</h2>
        <p>{window._env_.API_URL}</p>
        <div>
          <Link to='/read'>
            <Button>Read</Button>
          </Link>
          <Link to='/create'>
            <Button>Create</Button>
          </Link>
          <Route exact path='/create' component={Create} />
        </div>
        <div style={{ marginTop: 20 }}>
          <Route exact path='/read' component={Read} />
        </div>

        <Route path='/update' component={Update} />
      </div>
    </Router>
  );
}

export default App;