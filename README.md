# DAKKON Abdelmonim Project

## Starting the project
```
git clone git@gitlab.com:AbdellM/abdelmonim-dakkon-project.git
cd abdelmonim-dakkon-project/
docker-compose up
```
## Open http://localhost/ in Browser

## Home Page
![alt text](https://user-images.githubusercontent.com/71790032/219865024-14251b27-c62d-461f-82c5-8418a37db892.png)

## Create User Page
![alt text](https://user-images.githubusercontent.com/71790032/219865103-450fe8a1-5b04-4467-8018-99c7294d1898.png)

## Read Users Page
![alt text](https://user-images.githubusercontent.com/71790032/219865146-6163becf-1b9d-4218-8cf4-e737b7a6464c.png)
